# Firewalld-bash

This is just a simple bash script for collecting, adjusting, and quickly executing changes to a firewall ruleset.  Below are some other suggestions for things that can be added to the iprules.sh script.  This should be placed before port rules.

```bash
# Add service or port for a specific ip
#echo "* allowing sftp on port 115"
#$IPT --permanent --zone=public --add-rich-rule='
#  rule family="ipv4"
#  source address="1.1.1.1"
#  port protocol="tcp"
#  port="115"
#  accept'

# Another method (maybe better) is to add a port for a specific IP in a new zone
#$IPT --permanent --zone=public --remove-service=ssh
#$IPT --permmanent --zone=trusted --add-source=192.168.0.0/28
#$IPT --permanent --zone=trusted --add-service=ssh

# Alternatively...
# Add custom zones.  Multiple zones can not contain the same network.  However, 
# networks may specify smaller subnets already defined in other zones, 
# e.g., Zone1 may specify 192.168.1.0/24, even if Zone2 already 
# specifies 192.168.0.0/16.

#$IPT --permanent --new-zone-from-file=zones/newzone.xml
```

Alternatively, one should be able to limit connections with iptables.  However, fail2ban may be a better option in many cases.  This method also doesn't seem to work properly on all systems.

```
$IPT --permanent --zone=public --direct --add-rule ipv4 filter INPUT_direct 0 -p tcp --dport 22 -m state --state NEW -m recent --set
$IPT --permanent --zone=public --direct --add-rule ipv4 filter INPUT_direct 1 -p tcp --dport 22 -m state --state NEW -m recent --update --seconds 3600 --hitcount 4 -j LOG
#$IPT --permanent --zone=public --direct --add-rule ipv4 filter INPUT_direct 2 -p tcp --dport 22 -m state --state NEW -m recent --update --seconds 3600 --hitcount 4 -j REJECT --reject-with tcp-reset
$IPT --permanent --zone=public --direct --add-rule ipv4 filter INPUT_direct 2 -p tcp --dport 22 -m state --state NEW -m recent --update --seconds 3600 --hitcount 4 -j DROP
```
