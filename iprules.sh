#!/bin/bash

# Variables
IPT=/usr/bin/firewall-cmd
#source ipspaces

# Stupid way of resetting firewalld public zone
/usr/bin/rm -rf /etc/firewalld/zones/*
/usr/bin/rm -rf /etc/firewalld/direct.xml*
$IPT --reload

# Setup sources
$IPT --permanent --new-zone-from-file=zones/newzone.xml

# Open the following ports
# NOTE: This assumes one network interface with default "public" zone

echo "* setting ssh on port 22"
$IPT --permanent --zone=public --remove-service=ssh
$IPT --permanent --zone=newzone --add-service=ssh

echo "* setting http on port 80"
$IPT --permanent --zone=public --add-service=http

echo "* allowing kdeconnect"
$IPT --permanent --zone=public --add-service=kde-connect

# Reload iptables
echo "* reloading iptables"
$IPT --reload
